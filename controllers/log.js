const Log = require('../models/log');
const _ = require('lodash');
const path = require('path');

app.get('/log', function (req, res) {
    res.sendFile(path.resolve('./client/log.html'));
});

app.get('/log/full/:limit*?', function (req, res) {
    Log.find({})
        .sort('-date')
        .limit(req.params.limit ? req.params.limit * 1 : 100)
        .exec((e, all) => {
            res.json(all)
        });
});

app.get('/log/error/:limit*?', function (req, res) {
    Log.find({type: 'error'})
        .sort('-date')
        .limit(req.params.limit ? req.params.limit * 1 : 100)
        .exec((e, all) => {
            res.json(all)
        });
});