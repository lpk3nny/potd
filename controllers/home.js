const Post = require('../models/post');
const Log = require('../models/log');
const Resource = require('../models/resource');
const _ = require('lodash');
const path = require('path');

app.get('/', function (req, res) {
	res.sendFile(path.resolve('./client/app.html'));
});

app.get('/posts/:limit', function (req, res) {
	Post.find({})
		.sort('-syncDate')
		.limit(req.params.limit * 1)
		.exec((e, all) => {
			res.json(all)
		});
});

app.get('/resources/', function (req, res) {
	Resource.find({})
		.exec((e, all) => {
			res.json(all)
		});
});

app.post('/resources/', function (req, res) {
	Resource.create(req.body, function (err, model) {
		if (err) {
			Log.create({
				type:  'error',
				date:  new Date,
				value: {
					message: err.message,
					trace:   err.stack
				}
			});

			return res.json({success: false});
		}

		app.services.crawler.restartCrawling();
		res.json(model);
	});
});

app.put('/resources/:id', function (req, res) {
	Resource.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, model) {
		if (err) {
			Log.create({
				type:  'error',
				date:  new Date,
				value: {
					message: err.message,
					trace:   err.stack
				}
			});

			return res.json({success: false});
		}

		app.services.crawler.restartCrawling();
		res.json(model);
	});
});

app.delete('/resources/:id', function (req, res) {
	Resource.remove({_id: req.params.id}, function (err, model) {
		if (err) {
			Log.create({
				type:  'error',
				date:  new Date,
				value: {
					message: err.message,
					trace:   err.stack
				}
			});

			return res.json({success: false});
		}

		app.services.crawler.restartCrawling();
		res.json({success: true});
	});
});