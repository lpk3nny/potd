const _ = require('lodash');
const path = require('path');
const request = require('request');
const atob = require('atob');

app.get('/proxy/:path', function (req, res) {
    request.get(atob(req.params.path)).pipe(res);
});