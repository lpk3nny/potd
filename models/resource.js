const mongoose = require('mongoose');
const schema = mongoose.Schema({
	name:      String,
	type:      String,
	timeout:   Number,
	postCount: Number,
	imageSelector: String,
nextScreenSelector: String
});

module.exports = mongoose.model('resource', schema);