const mongoose = require('mongoose');
const schema = mongoose.Schema({
	type:  String,
	date:  {type: Date, index: true},
	value: Object
});

module.exports = mongoose.model('log', schema);