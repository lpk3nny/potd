const mongoose = require('mongoose');
const schema = mongoose.Schema({
    sections:   [{
        img:  {type: String, index: {unique: true}},
        desc: String
    }],
    sourceUrl:  String,
    sourceName: String,
    title:      String,
    lead:       String,
    desc:       String,
    syncDate:   Date
});

module.exports = mongoose.model('post', schema);