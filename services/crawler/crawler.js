'use strict';
const config = require('../../config');
const _ = require('lodash');

const Post = require('../../models/post');
const Log = require('../../models/log');
const Resource = require('../../models/resource');

const instagramStrategy = require('./strategies/instagram');
const tumblrStrategy = require('./strategies/tumblr');
const webStrategy = require('./strategies/web');

module.exports = class {
	constructor() {
		this.intervals = [];
		this.starCrawling();
	}

	starCrawling() {
		this.restartCrawling();
	}

	restartCrawling() {
		this.resources = Resource
			.find({})
			.exec((err, resources) => {
				if (err) return Log.create({
					type:  'error',
					date:  new Date,
					value: {
						message: err.message,
						trace:   err.stack
					}
				}, (e)=> {
					if (e)
						console.error(e)
				});

				this.clearIntervals();
				_.each(resources, (resource) => {
                    var strategy;

                    switch (resource.type) {
						case 'tumblr':
							strategy = tumblrStrategy;
							break;
						case 'instagram':
							strategy = instagramStrategy;
							break;
                        case 'web':
                            strategy = webStrategy;
                            break;
					}

					new strategy(resource, this.storePost);
					this.intervals.push(setInterval(() => {
						new strategy(resource, this.storePost);
					}, 1000 * 60 * resource.timeout));
				});
			});
	}

	clearIntervals() {
		_.each(this.intervals, (interval) => {
			clearInterval(interval);
		});

		this.intervals = [];
	}

	storePost(post) {
		var post = new Post(post);
		console.log(post.sourceUrl, 'fetched');

		post.save((err) => {
			if (err) return Log.create({
				type:  'error',
				date:  new Date,
				value: {
					message: err.message,
					trace:   err.stack
				}
			}, (e)=> {
				if (e)
					console.error(e)
			});

			Log.create({
				type:  'success',
				date:  new Date,
				value: {
					msg: post.sourceName + ' saved'
				}
			});
		});
	}
};