'use strict';
const http = require('http');
const path = require('path');
const _ = require('lodash');
const config = require('../../../config');
const Log = require('../../../models/log');
const Nightmare = require('nightmare');

module.exports = class {
    constructor(settings, saveCallback) {
        this.settings = settings;
        this.saveCallback = saveCallback;
        this.photoCounter = 0;

        this.makeRequest(this.settings.name);
    }

    makeRequest(url) {
        const nightmare = Nightmare({
            // show: true,
            // openDevTools: {
            //     mode: 'detach'
            // },
            webPreferences: {
                partition: 'persist: cache'
            }
        });

        nightmare
            .goto(url)
            .inject('js', path.resolve(__dirname + '/../assets/jquery-3.2.1.js'))
            .evaluate(function (settings) {
                const photos = [];
                $(settings.imageSelector).each((key, el) => {
                    photos.push($(el).attr('src'));
                });
                return {
                    photos: photos,
                    nextUrl: $(settings.nextScreenSelector).attr('href')
                };
            }, this.settings)
            .end()
            .then(result => {
                this.photoCounter += result.photos.length;

                this.save(result.photos);
                if (this.photoCounter < this.settings.postCount)
                    this.makeRequest(result.nextUrl)
            })
            .catch(error => {
                Log.create({
                    type: 'error',
                    date: new Date,
                    value: {
                        message: error
                    }
                }, (e) => {
                    if (e)
                        console.error(e)
                });
            });
    }

    save(photos) {
        _.each(photos, (photo) => {
            this.saveCallback({
                sourceUrl: this.settings.name,
                sourceName: this.settings.name,
                syncDate: new Date,
                title: null,
                desc: null,
                sections: [{
                    img: photo,
                }]
            });
        });
    }
};