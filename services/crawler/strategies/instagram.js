'use strict';
const https = require('https');
const _ = require('lodash');
const Log = require('../../../models/log');

module.exports = class {
	constructor(settings, saveCallback) {
		this.settings = settings;
		this.saveCallback = saveCallback;
		this.apiURL = 'https://www.instagram.com/' + settings.name + '/media/';

		this.makeRequest();
	}

	makeRequest() {
		https.get(this.apiURL, (res) => {
			if(res.statusCode !== 200)
				Log.create({
					type:  'error',
					date:  new Date,
					value: {
						message: `${this.settings.name} instagram strategy fails with ${res.statusCode} code`
					}
				}, (e)=> {
					if (e)
						console.error(e)
				});
			
			let body = '';
			res.on('data', (chunk) => {
				body += chunk;
			});
			res.on('end', () => {
				try {
					this.save(JSON.parse(body));
				} catch(err) {
					Log.create({
						type:  'error',
						date:  new Date,
						value: {
							message: err.message,
							trace:   err.stack
						}
					}, (e)=> {
						if (e)
							console.error(e)
					});
				}				
			});
		}).on('error', (err) => {
			Log.create({
				type:  'error',
				date:  new Date,
				value: {
					message: err.message,
					trace:   err.stack
				}
			}, (e)=> {
				if (e)
					console.error(e)
			});
		});
	}

	save(body) {
		if (!body.status || body.status !== 'ok')
			return;

		_.each(body.items, (post)=> {
			if (post.type !== 'image')
				return;

			this.saveCallback({
				sourceUrl:  post.link,
				sourceName: post.user.username,
				syncDate:   new Date,
				desc:       post.caption && post.caption.text ? post.caption.text : null,
				sections:   [{
					img: post.images.standard_resolution.url
				}]
			});
		});
	}
};