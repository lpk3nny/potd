'use strict';
const http = require('http');
const _ = require('lodash');
const config = require('../../../config');
const Log = require('../../../models/log');

module.exports = class {
    constructor(settings, saveCallback) {
        this.settings = settings;
        this.saveCallback = saveCallback;
        this.apiURL = 'http://api.tumblr.com/v2/blog/' + settings.name
            + '/posts?limit=' + settings.count
            + '&api_key=' + config.tumbrlKey;

        this.makeRequest();
    }

    makeRequest() {
        http.get(this.apiURL, (res) => {
            if(res.statusCode !== 200)
                Log.create({
                    type:  'error',
                    date:  new Date,
                    value: {
                        message: `${this.settings.name} tumblr strategy fails with ${res.statusCode} code`
                    }
                }, (e)=> {
                    if (e)
                        console.error(e)
                });

            let body = '';
            res.on('data', (chunk) => {
                body += chunk;
            });
            res.on('end', () => {
                try {
                    this.save(JSON.parse(body));
                } catch(err) {
                    Log.create({
                        type:  'error',
                        date:  new Date,
                        value: {
                            message: err.message,
                            trace:   err.stack
                        }
                    }, (e)=> {
                        if (e)
                            console.error(e)
                    });
                }
            });
        }).on('error', (e) => {
            Log.create({
                type:  'error',
                date:  new Date,
                value: {
                    message: err.message,
                    trace:   err.stack
                }
            }, (e)=> {
                if (e)
                    console.error(e)
            });
        });
    }

    save(body) {
        _.each(body.response.posts, (post)=> {
            let sections = [];

            _.each(post.photos, (photo)=> {
                sections.push({
                    img:  photo.original_size.url,
                    desc: photo.caption
                })
            })

            this.saveCallback({
                sourceUrl:  post.post_url,
                sourceName: post.blog_name,
                syncDate:   new Date,
                title:      post.summary ? post.summary : null,
                desc:       post.caption ? post.caption : null,
                sections:   sections
            });
        });
    }
};