define(['virtualbone', '../../collections/log', 'text!pages/logs/logs.html'], function (Backbone, collection, tpl) {
	return Backbone.View.extend({
		el:         document.querySelector('.page-log'),
		template:   _.template(tpl),
		initialize: function () {
			this.collection = new collection;
			this.listenTo(this.collection, 'sync', this.render);
		},
		render:     function () {
			this.patch(this.template(this.collection));
		}
	});
});