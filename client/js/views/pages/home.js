define(['virtualbone',
	'../../collections/posts',
	'../components/post',
	'text!pages/home/home.html',
	'../../collections/resources',
	'../components/resource',
], function (Backbone, Collection, Post, tpl, resourcesCollection, Resource) {
	return Backbone.View.extend({
		el:         document.querySelector('.page-home'),
		template:   _.template(tpl),
		events:     {
			'click .resources .add': 'addResource',
			'click .menu-handle':    'showMenu',
			'click .overlay':        'hideMenu'
		},
		initialize: function () {
			this.collection = new Collection;
			this.collection.on('add', this.appendPostView.bind(this));

			this.resourcesCollection = new resourcesCollection;
			this.resourcesCollection.on('add', this.appendResource.bind(this));

			this.render();
		},

		render: function () {
			this.patch(this.template());
		},

		appendPostView: function (model) {
			var post = new Post({
				model: model
			});

			this.$el.find('.posts').append(post.el);
			post.render();
		},

		appendResource: function (model) {
			var resource = new Resource({
				model: model
			});

			this.$el.find('.resources .list').append(resource.el);
			resource.render();
			resource.$el.addClass('show');
		},

		addResource: function () {
			this.resourcesCollection.add({});
		},

		showMenu: function () {
			$('body').toggleClass('menu-active');

			var timeout = 0;
			_.each(this.$el.find('.resources .resource'), function (resource) {
				var $resource = $(resource);

				timeout++;
				setTimeout(function () {
					$resource.addClass('show');
				}, timeout * 150)
			})
		},

		hideMenu: function () {
			$('body').removeClass('menu-active');

			this.$el.find('.resources .resource').removeClass('show');
		}
	});
});