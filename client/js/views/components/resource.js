define(['virtualbone', 'underscore', 'text!pages/home/resource.html'], function (Backbone, _, tpl) {
	return Backbone.View.extend({
		className: 'resource',
		template:  _.template(tpl),
		events:    {
			'keyup [contenteditable]': 'changeModel',
			'change select':           'changeModel',
			'click .save':             'saveModel',
			'click .remove':           'destroyModel'
		},
		isShown:   false,

		initialize: function () {
			this.listenTo(this.model, 'change', this.render.bind(this));
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function () {
			this.patch(this.template({
				isShown: this.isShown,
				model:   this.model
			}));
		},

		changeModel: function () {
			this.isShown = true;
			this.model.set({
				name:      this.$el.find('.name').text(),
				type:      this.$el.find('.type').val(),
                imageSelector: this.$el.find('.image-selector').text(),
                nextScreenSelector: this.$el.find('.next-page-selector').text(),
				postCount: this.$el.find('.post-count').text(),
				timeout:   this.$el.find('.timeout').text()
			});
		},

		destroyModel: function () {
			this.model.destroy();
		},

		saveModel: function () {
			this.model.save();
		}
	});
});