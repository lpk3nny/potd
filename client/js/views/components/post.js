define(['virtualbone', 'underscore', 'text!pages/home/post.html'], function (Backbone, _, tpl) {
	return Backbone.View.extend({
		className:  'post',
		template:   _.template(tpl),
		events:     {
			'click img': 'enableMaxHeightMode'
		},
		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
		},

		render: function () {
			this.patch(this.template({
				post: this.model
			}));

			this.setSpecialLayoutClass();
		},

		setSpecialLayoutClass: function () {
			this.$el.addClass('special-' + _.random(1, 3));

			if (this.model.get('desc') && this.model.get('desc').length < 200)
				this.$el.addClass('small-desc');
		},

		enableMaxHeightMode: function (e) {
			$img = $(e.target);

			$img.css('max-height', window.innerHeight);
		},
	});
});