define(['../models/log-entry', 'virtualbone'], function (model, Backbone) {
    return Backbone.Collection.extend({
        url:        '/log/full/200',
        model:      model,
        initialize: function () {
            this.fetch();
        }
    });
})