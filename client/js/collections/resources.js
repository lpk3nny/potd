define(['../models/resource', 'virtualbone'], function (model, Backbone) {
    return Backbone.Collection.extend({
        url:        '/resources/',
        model:      model,
        initialize: function () {
            this.fetch();
        }
    });
})