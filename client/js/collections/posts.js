define(['../models/post', 'virtualbone'], function (model, Backbone) {
    return Backbone.Collection.extend({
        url:        '/posts/100',
        model:      model,
        initialize: function () {
            this.fetch();
        }
    });
})