requirejs.config({
	baseUrl: '/',
	paths:   {
		text:        '/text/text',
		app:         '/js/common/app',
		backbone:    '/backbone/backbone',
		underscore:  '/underscore/underscore',
		'set-dom':   '/set-dom/dist/set-dom',
		virtualbone: '/virtualbone/dist/virtualbone',
		jquery:      '/jquery/dist/jquery',
		moment:      '/moment/moment'
	}
});