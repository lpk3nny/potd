define(['virtualbone'], function (Backbone) {
	return Backbone.Model.extend({
		idAttribute: '_id',
		url:         function () {
			return '/resources/' + (this.get('_id') ? this.get('_id') : '');
		},
		defaults:    {
			name:      '',
			type:      'tumblr',
			timeout:   60,
			postCount: 20
		}
	});
})