define(['virtualbone', 'moment'], function (Backbone, moment) {
    return Backbone.Model.extend({
        getFormattedDate: function () {
            return moment(this.get('date')).calendar();
        },
        getMesssage: function() {
            return this.get('value').msg || this.get('value').message;
        }
    });
})