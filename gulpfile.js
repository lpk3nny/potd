var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
const changed = require('gulp-changed');

gulp.task('default', function () {
    gulp.start('less');
    gulp.start('js');

    gulp.watch('./client/less/**/*.less', ['less']);
    gulp.watch('./client/js/**/*.js', ['js']);
});

gulp.task('prod', function () {
    gulp.start('less');
    gulp.start('js');
});

gulp.task('less', function () {
    return gulp.src('./client/less/main.less')
        .pipe(less())
        .pipe(gulp.dest('./client/build/css/'));
});

gulp.task('js', function () {
    return gulp.src('./client/js/**/*.js')
        .pipe(changed('./client/build/js/'))
        .pipe(gulp.dest('./client/build/js/'));
});
