const Express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const db = mongoose.connection;

const crawler = require('./services/crawler/crawler');

global.app = Express();
app.use(bodyParser.json());

db.on('error', console.error.bind(console, 'Connection to db error:'));
db.once('open', () => {
    console.log('Connected to db!')
});
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://lpk3nny:here677FYYY343@78.155.217.201/podt');

//controllers
require('./controllers/home');
require('./controllers/log');
require('./controllers/proxy');

app.use(Express.static('./client/build'));
app.use(Express.static('./client/templates'));
app.use(Express.static('./client/meta'));
app.use(Express.static('./node_modules'));
app.services = {
    crawler: new crawler
};

app.listen(3000, function () {
    console.log('noise app listening on port 3000!');
});